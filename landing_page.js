document.getElementsByClassName("sticky-note").onmousemove = (e) => {
  for (const card of document.getElementsByClassName("sticky-note")) {
    const rect = card.getBoundingClientRect(),
      x = e.clientX - rect.left,
      y = e.clientY - rect.top;

    card.style.setProperty("--mouse-x", `${x}px`);
    card.style.setProperty("--mouse-y", `${y}px`);
  }
  console.log("tt");
};

let sticky = document.querySelector(".note-4");

sticky.addEventListener("mousedown", (event) => {
  console.log("test");
});

let sticky1 = document.getElementsByClassName("sticky-note");

function isTouchDevice() {
  try {
    document.createEvent("TouchEvent");
    return true;
  } catch (e) {
    return false;
  }
}

const move = (e) => {
  try {
    var x = !isTouchDevice() ? e.pageX : e.touches[0].pageX;
    var y = !isTouchDevice() ? e.pageY : e.touches[0].pageY;
  } catch (e) {}
  sticky1.style.left = x + "px";
  sticky1.style.top = y + "px";
};

document.addEventListener("mousemove", (e) => {
  move(e);
});
